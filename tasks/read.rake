require 'csv'
require 'open-uri'

namespace :nrc_data do
  desc "Download and Read into the DB reactor status"
  task :read  do
    csv_text = open('http://www.nrc.gov/reading-rm/doc-collections/event-status/reactor-status/PowerReactorStatusForLast365Days.txt')
    csv = CSV.parse(csv_text, {:headers => true, :col_sep => "|"})

    csv.each do |row|
      puts row
      ReactorStatus.create!(row.to_hash)
    end
  end
end

