class ChangeToDate < ActiveRecord::Migration
  def change
    change_column :reactor_statuses, :ReportDt, :date
  end
end
