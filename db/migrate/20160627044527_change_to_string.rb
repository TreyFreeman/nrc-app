class ChangeToString < ActiveRecord::Migration
  def change
    change_column :reactor_statuses, :ReportDt, :string
  end
end
