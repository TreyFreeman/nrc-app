class CreateReactorStatus < ActiveRecord::Migration
  def up
    create_table :reactor_statuses do |t|
      t.datetime :ReportDt
      t.string   :Unit
      t.integer  :Power
    end
  end

  def down
    drop_table :reactor_statuses
  end
end
