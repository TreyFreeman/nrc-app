require 'sinatra'
require 'sinatra/activerecord'
require './config/environments' #database configuration
require './models/model'

set :views, File.dirname(__FILE__) + '/views'
set :public_folder, File.dirname(__FILE__) + '/public_folder'

get '/' do
  @statuses = ReactorStatus.all
  erb :index
end
